class BookingRejections < ActiveRecord::Migration
  def change
    create_table :booking_rejections do |t|
      t.belongs_to :taxi
      t.belongs_to :booking

      t.timestamps
    end
  end
end
