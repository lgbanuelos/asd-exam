# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bus_station, class: Taxi do
    driver_name "Juan Perez"
    current_address "Kaluri 2"
  end
  factory :hospital, class: Taxi do
    driver_name "Jose Perez"
    current_address "Puusepa 11"
  end
end
