require 'spec_helper'

describe TaxiAllocatorJob do

  it 'should immediately execute a booking assignment' do
    booking = create(:booking)
    TaxiAllocatorJob.new.async.perform(booking.id)
    expect( Booking.find(booking.id).status ).to eql('FORWARDED')
  end

  it 'should eventually execute a deferred booking assignment' do
    booking = create(:booking)
    TaxiAllocatorJob.new.async.perform_later(5, booking.id)
    expect( Booking.find(booking.id).status ).to eql(nil)
    sleep 7
    expect( Booking.find(booking.id).status ).to eql('FORWARDED')
  end

  it 'should select one taxi' do
    taxi1 = create(:bus_station)
    taxi2 = create(:hospital)
    booking = create(:booking) # It is assumed I am in front PlayTech building

    json = TaxiAllocatorJob.new.select_taxi(booking)

    expect(Booking.find(booking.id).taxi).to eql(taxi1)
  end
end