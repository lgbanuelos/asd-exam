# Setting up the project

Of course, you should start by installing all the required gems. Do this by means of the usual command:

```sh
bundle install
```

Please note that I updated rails to its version `3.1.18`, because in Windows rails 3.1.17 is not available.


```sh
rake db:migrate
```