'use strict'

app = angular.module('taxiHomeApp')

app.service 'STRSService', (btfModal, AsyncNotifModal, $http) ->
  bookings = []
  booking = {}

  pusher = new Pusher('a5413d7514e523255c2e')
  channel = pusher.subscribe('strs')
  channel.bind 'booking_request', (data) ->
    booking = data
    AsyncNotifModal.activate()

  notifyDecision: (decision) ->
    booking.status = decision
    $http.post('http://localhost:3000/taxiAssignments', {'booking': booking})
    bookings.splice 0, 0, booking
    AsyncNotifModal.deactivate()

  activateModal: -> AsyncNotifModal.activate()
  getBooking: -> booking
  getBookings: -> bookings
