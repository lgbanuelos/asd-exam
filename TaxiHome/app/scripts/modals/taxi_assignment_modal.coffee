'use strict'

app = angular.module('taxiHomeApp')

app.controller 'AsyncNotifModalController', ($scope, STRSService) ->
  booking = STRSService.getBooking()
  $scope.name = booking.name
  $scope.address = booking.address
  $scope.phone = booking.phone

  $scope.accept = ->
    STRSService.notifyDecision('ACCEPTED')

  $scope.reject = ->
    STRSService.notifyDecision('REJECTED')

app.factory 'AsyncNotifModal', (btfModal) ->
  btfModal {
    controller: 'AsyncNotifModalController'
    controllerAs: 'modal'
    templateUrl: 'views/modal.html'
  }
