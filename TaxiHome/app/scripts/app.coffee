'use strict'

angular
  .module('taxiHomeApp', [
    'ngResource',
    'ngRoute',
    'btford.modal'
  ])
  .config ($routeProvider) ->
    $routeProvider
      .when '/',
        templateUrl: 'views/main.html'
        controller: 'MainCtrl'
      .otherwise
        redirectTo: '/'

