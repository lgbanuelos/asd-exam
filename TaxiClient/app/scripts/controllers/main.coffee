'use strict'

app = angular.module('taxiClientApp')
app.controller 'MainCtrl', ($scope, BookingsSyncService) ->
  $scope.name = ''
  $scope.phone = ''
  $scope.pickupAddress = ''
  $scope.syncNotification = ''
  $scope.submit = ->
    BookingsSyncService.save {
      'customer_name': $scope.name
      'phone': $scope.phone
      'pickup_address': $scope.pickupAddress
    }, (data) ->
      $scope.syncNotification = data