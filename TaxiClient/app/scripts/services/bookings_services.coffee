'use strict'

app = angular.module('taxiClientApp')

app.service 'BookingsSyncService', ($rootScope, $http) ->
  lastBookingId = 0
  save: (bookingDetails, callback) ->
    $http.post('http://localhost:3000/bookings', {booking: bookingDetails})
      .success (data, status) ->
        lastBookingId = data.bookingId
        callback( data.message )
  getLastBookingId: ->
    lastBookingId
